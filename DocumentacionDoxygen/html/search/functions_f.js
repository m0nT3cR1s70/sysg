var searchData=
[
  ['tim',['tim',['../class_b_i_c_g_s_t_a_b.html#a6a09944cd69150098c82760d058d4857',1,'BICGSTAB::tim()'],['../class_c_g.html#ab1de59d8f4f6aa5c04a604db85bb7b2a',1,'CG::tim()'],['../class_gauss_seidel.html#a78d7823a16dd20eb30452ef340320a72',1,'GaussSeidel::tim()'],['../class_jacobi.html#a949ec5de87b55889cb3e69400e1e50b7',1,'Jacobi::tim()'],['../class_s_o_r.html#aa592907de2e098a9f13de1ce08fdc00e',1,'SOR::tim()']]],
  ['tole',['tole',['../class_b_i_c_g_s_t_a_b.html#ab0a72020c9ea0f2fecf806e973c1b41e',1,'BICGSTAB::tole()'],['../class_c_g.html#a2153a35147089adbfb44eb1ec99ae982',1,'CG::tole()'],['../class_gauss_seidel.html#a6df722abff1a051a0659a42224ff28c3',1,'GaussSeidel::tole()'],['../class_jacobi.html#a43d8a58638f07dc6c427251efe5d6af8',1,'Jacobi::tole()'],['../class_s_o_r.html#a733204031026cfad302c9d1ced3b7cb5',1,'SOR::tole()']]],
  ['type',['type',['../class_mesh.html#aef283d305e17ab48771999e7835ed294',1,'Mesh']]],
  ['typenodeborder',['typeNodeBorder',['../class_mesh.html#a1e306db2532f24a66b8d43cb634afd61',1,'Mesh']]]
];
